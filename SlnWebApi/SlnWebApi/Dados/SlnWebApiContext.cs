﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SlnWebApi.Models;


namespace SlnWebApi.Dados
{
    public class SlnWebApiContext : DbContext
    {
        public SlnWebApiContext(DbContextOptions<SlnWebApiContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FuncionarioProjeto>()
                .HasKey(ac => new { ac.FuncionarioId, ac.ProjetoId });
        }
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<Projeto> Projetos { get; set; }
        public DbSet<Setor> Setores { get; set; }
        public DbSet<SlnWebApi.Models.FuncionarioProjeto> FuncionarioProjeto { get; set; }

    }
}
