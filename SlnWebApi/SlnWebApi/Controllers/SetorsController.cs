﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SlnWebApi.Dados;
using SlnWebApi.Models;

namespace SlnWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SetorsController : ControllerBase
    {
        private readonly SlnWebApiContext _context;

        public SetorsController(SlnWebApiContext context)
        {
            _context = context;
        }

        // GET: api/Setors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Setor>>> GetSetores()
        {
            return await _context.Setores.ToListAsync();
        }

        // GET: api/Setors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Setor>> GetSetor(int id)
        {
            var setor = await _context.Setores.FindAsync(id);

            if (setor == null)
            {
                return NotFound();
            }

            return setor;
        }

        // PUT: api/Setors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSetor(int id, Setor setor)
        {
            if (id != setor.Id)
            {
                return BadRequest();
            }

            _context.Entry(setor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SetorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Setors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Setor>> PostSetor(Setor setor)
        {
            _context.Setores.Add(setor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSetor", new { id = setor.Id }, setor);
        }

        // DELETE: api/Setors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Setor>> DeleteSetor(int id)
        {
            var setor = await _context.Setores.FindAsync(id);
            if (setor == null)
            {
                return NotFound();
            }

            _context.Setores.Remove(setor);
            await _context.SaveChangesAsync();

            return setor;
        }

        private bool SetorExists(int id)
        {
            return _context.Setores.Any(e => e.Id == id);
        }
    }
}
